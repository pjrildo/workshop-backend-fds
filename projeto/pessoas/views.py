from django.shortcuts import render, redirect
from .models import Pessoa


def listar_pessoas(request):
    pessoas = Pessoa.objects.all()
    return render(request, 'pessoas/listar_pessoas.html', {'object_list': pessoas})


def criar_pessoa(request):
    if request.method == 'GET':
        return render(request, 'pessoas/criar_pessoa.html')
    else:  # POST
        nome = request.POST['nome']
        idade = request.POST['idade']
        if int(idade) < 0:
            return render(request, 'pessoas/criar_pessoa.html', {'erro': 'Idade não pode ser menor que zero'})
        pessoa = Pessoa(nome=nome, idade=idade)
        pessoa.save()
        return redirect('/pessoas')
