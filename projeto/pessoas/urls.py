from django.urls import path
from .views import listar_pessoas, criar_pessoa

urlpatterns = [
    path('', listar_pessoas),
    path('criar/', criar_pessoa)
]
